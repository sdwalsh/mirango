---
layout: page
title: About
permalink: /about/
---
Hey!  I'm Sean and this is my website and blog.

I'm a computer science student in Boston with a strong interest in functional programming (particularly OCaml and Scala), web development, and concurrent programming.  You'll find me on [gitlab][gitlab] and [twitter][twitter].

I've becoming increasingly interested in web development, splitting my time between ruby on rails, play, nodejs, and angularjs.

Feel free to send me an email at sdwalsh [at] mirango.io and consider using my [pgp key][pgpkey].

Mirango.io content is served using Heroku!

[twitter]: https://www.twitter.com/mirangomadness
[gitlab]: https://gitlab.com/u/sdwalsh
[pgpkey]: https://pgp.mit.edu/pks/lookup?op=get&search=0xE864A72A086E2C05
