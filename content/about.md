## About

Hey! I’m Sean and this is my website and blog.

I’m a software developer in Boston with a strong interest in web development, functional programming, and concurrent programming. You’ll find me on [`bitbucket`][bitbucket], [`keybase`][keybase], or [`twitter`][twitter].

While working on web projects I split my time between ruby on rails, nodejs, and go.

Feel free to send me an email at `sdwalsh@mirango.io` and consider using my pgp key.

[bitbucket]: https://www.bitbucket.org/sdwalsh
[keybase]: https://keybase.io/sdwalsh
[twitter]: https://www.twitter.com/mirangomadness