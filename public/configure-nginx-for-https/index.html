<!DOCTYPE html>
<html lang="en-us">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Configure Nginx while avoiding common pitfalls and vulnerabilities. While setting up `www.mirango.io`, I chose to adopt HTTPS in order to ensure privacy of readers.  Here&#39;s a short guide I put together while setting up my server.
">
  <meta name="generator" content="Hugo 0.40.1" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/main.css" type="text/css">
  <link rel="alternate" href="/index.xml" type="application/rss+xml" title="mirango">
  <title>Properly Configuring Nginx for HTTPS - mirango</title>
</head>



<body>
<div class="container">
  <div class="header">
  
    <h1 class="site-title mirango">
      
<a href="https://www.mirango.io/"><span class="base05">[</span><span class="base08">m</span><span class="base09">i</span><span class="base0a">r</span><span class="base0b">a</span><span class="base0c">n</span><span class="base0d">g</span><span class="base0e">o</span><span class="base05">]</span></a>
    </h1>
    <nav>
  <a href="/about">about</a>
  <a href="https://www.bitbucket.org/sdwalsh">bitbucket</a>
  <a href="https://www.keybase.io/sdwalsh">keybase</a>
  <a href="https://www.twitter.com/mirangomadness">twitter</a>
</nav>
  </div>

<div class="content">
  

<h1 id="properly-configuring-nginx-for-https">Properly Configuring Nginx for HTTPS</h1>

<p>While setting up www.mirango.io, I chose to adopt HTTPS in order to ensure privacy of readers.  Here&rsquo;s a short guide I put together while setting up my server.</p>

<h2 id="http-to-https-redirects">HTTP to HTTPS redirects</h2>

<p>In order to enforce HTTPS, it&rsquo;s necessary to redirect all HTTP traffic (port 80) to HTTPS (port 443).  On Nginx servers, this is accomplished within the server block:</p>
<div class="highlight"><pre class="chroma"><code class="language-Nginx" data-lang="Nginx"><span class="k">server</span> <span class="p">{</span>
       <span class="kn">listen</span>         <span class="mi">80</span><span class="p">;</span>
       <span class="kn">server_name</span>    <span class="s">www.mirango.com</span><span class="p">;</span>
       <span class="c1"># 301 Permanent Redirect
</span><span class="c1"></span>       <span class="kn">return</span>         <span class="mi">301</span> <span class="s">https://www.mirango.io</span><span class="nv">$request_uri</span><span class="p">;</span>
    <span class="p">}</span></code></pre></div>
<p>When users visit <code>http://www.mirango.com:80</code> they receive a <code>301 Permanent Redirect</code> to <code>https://www.mirango.io:443</code>.  301 redirects are recommended by both Nginx and <a href="https://support.google.com/webmasters/answer/93633?hl=en">Google</a> to redirect traffic from any non-preferred domain.  Similar code is used to specify the canonical domain for mirango.io (www over non-www).</p>

<h2 id="ocsp-stapling">OCSP Stapling</h2>

<p><a href="http://tools.ietf.org/html/rfc2560">OCSP</a>, Online Certificate Status Protocol, is a protocol used to check revocation status of certificates - specifically designed as an alternative to certificate revocation lists.  OCSP responders are generally run by certificate authorities, but are often slow and prone to failures.</p>

<p>Enter <a href="http://tools.ietf.org/html/rfc6066">OCSP stapling</a>.  Also known as TLS Certificate Status Request extension,  OCSP stapling allows web servers to cache the OCSP record clients normally receive from an OCSP responder, saving network resources.  The cached record is sent during the TLS handshake, but only if the client requests it during the extended ClientHello (status_request).</p>
<div class="highlight"><pre class="chroma"><code class="language-Nginx" data-lang="Nginx"><span class="k">ssl_stapling</span> <span class="no">on</span><span class="p">;</span>
<span class="k">ssl_stapling_verify</span> <span class="no">on</span><span class="p">;</span>
<span class="c1"># make sure full_chain.pem contains all certificates
</span><span class="c1"></span><span class="k">ssl_trusted_certificate</span> <span class="s">/etc/nginx/ssl/full_chain.pem</span><span class="p">;</span></code></pre></div>
<h2 id="http-strict-transport-security">HTTP Strict Transport Security</h2>

<p>With the threat of <a href="https://www.blackhat.com/presentations/bh-dc-09/Marlinspike/BlackHat-DC-09-Marlinspike-Defeating-SSL.pdf">downgrade attacks</a> on TLS (transparent conversion of HTTPS to HTTP in a MitM style attack), it is recommended to implement HTTP Strict Transport Securit (HSTS). HSTS enables a website to declare it is only accessible through secure connections.</p>

<p>To enable HSTS, simply add the following line to the server block:</p>
<div class="highlight"><pre class="chroma"><code class="language-Nginx" data-lang="Nginx"><span class="c1"># enable HSTS for one year on *.mirango.io
</span><span class="c1"></span><span class="k">add_header</span> <span class="s">Strict-Transport-Security</span> <span class="s">&#34;max-age=31536000</span><span class="p">;</span> <span class="k">includeSubDomains&#34;</span><span class="p">;</span></code></pre></div>
<h2 id="ssl-protocols-and-poodle">SSL Protocols and POODLE</h2>

<p>The threat from <a href="https://www.openssl.org/~bodo/ssl-poodle.pdf">POODLE</a>, Padding Oracle On Downgraded Legacy Encryption, has necessitated the retirement of SSLv3 support (TLSv1 actually replaced SSLv3 15 years ago!).  The vulnerability exploited by POODLE allows a MitM attacker to decrypt secure HTTP cookies using a padding oracle attack against CBC-mode (cipher block chaining) ciphers.</p>

<p>POODLE works since SSLv3 decrypts before verifying the MAC (used to detect tampering) and ignores the contents of padding bytes, only looking for the last byte containing the length of the padding.  For a more detailed explanation of the vulnerability that allows POODLE to work, <a href="http://www.educatedguesswork.org/2011/09/security_impact_of_the_rizzodu.html">click here</a>.</p>

<p>Securing a web server against this style attack is as simple as removing SSLv3 support.</p>
<div class="highlight"><pre class="chroma"><code class="language-Nginx" data-lang="Nginx"><span class="c1"># use TLSv1+
</span><span class="c1"></span><span class="k">ssl_protocols</span>           <span class="s">TLSv1</span> <span class="s">TLSv1.1</span> <span class="s">TLSv1.2</span><span class="p">;</span></code></pre></div>
<h2 id="public-key-pinning-extension-for-http-hpkp">Public Key Pinning Extension for HTTP (HPKP)</h2>

<p>HPKP, Public Key Pinning Extension, is a HTTP header that instructs clients to remember the server&rsquo;s cryptographic identity.  When a client visits the server again, it expects a certificate containing the public key stored from the original visit.  HPKP reduces the ability to <a href="http://arstechnica.com/business/2012/02/critics-slam-ssl-authority-for-minting-cert-used-to-impersonate-sites/">issue forged certificates</a> for a website.  It should be noted that there is an inherent weakness to key pinning. Since key pinning is a trust on first use security mechanism a client cannot detect a MitM attack (using a forged certificate) when visiting a site for the first time.</p>

<p>HPKP requires the base64-encode of SHA256 hash of the public key, certificate signing request, or the certificate.  <a href="https://developer.mozilla.org/en-US/docs/Web/Security/Public_Key_Pinning">Mozilla provides a short guide</a>:</p>
<div class="highlight"><pre class="chroma"><code class="language-bash" data-lang="bash"><span class="c1">#Given the public key my-key-file.key:
</span><span class="c1"></span>openssl rsa -in my-key-file.key -outform der -pubout <span class="p">|</span> openssl dgst -sha256 -binary <span class="p">|</span> openssl enc -base64

<span class="c1">#Given the CSR my-signing-request.csr:
</span><span class="c1"></span>openssl req -in my-signing-request.csr -pubkey -noout <span class="p">|</span> openssl rsa -pubin -outform der <span class="p">|</span> openssl dgst -sha256 -binary <span class="p">|</span> openssl enc -base64

<span class="c1">#Or given the certificate my-certificate.crt
</span><span class="c1"></span>openssl x509 -in my-certificate.crt -pubkey -noout <span class="p">|</span> openssl rsa -pubin -outform der <span class="p">|</span> openssl dgst -sha256 -binary <span class="p">|</span> openssl enc -base64</code></pre></div><div class="highlight"><pre class="chroma"><code class="language-Nginx" data-lang="Nginx"><span class="k">add_header</span> <span class="s">Public-Key-Pins</span> <span class="s">&#39;pin-sha256=&#34;BASE64-KEY-INFORMATION&#34;</span><span class="p">;</span> <span class="k">pin-sha256=&#34;BASE64-KEY-INFORMATION&#34;</span><span class="p">;</span> <span class="k">max-age=5184000</span><span class="p">;</span> <span class="k">includeSubDomains&#39;</span><span class="p">;</span></code></pre></div>
<h2 id="logjam-vulnerability-freak">Logjam Vulnerability / FREAK</h2>

<p>A recent <a href="https://weakdh.org/imperfect-forward-secrecy.pdf">paper</a> published by INRIA, Microsoft Research, University of Pennsylvania, Johns Hopkins, and University of Michigan exposes a flaw in TLS.  Logjam is similar to FREAK but, rther than an implementation vulnerability like FREAK, logjam exploits a vulnerability in the TLS protocol itself.</p>

<p>The vulnerability relies on old export ciphers that were developed during the 1990&rsquo;s when the United States banned the export of &ldquo;strong ciphers&rdquo; (<a href="https://en.wikipedia.org/wiki/Arms_Export_Control_Act">Arms Export Control Act</a>).  These export ciphers were specifically designed to be weak and today can be broken with home computers.</p>

<p>This leads into the vulnerability.</p>

<p>During a man in the middle attack, an attacker can intercept a client connection and replace accepted ciphers with DHE_EXPORT.  The server will then pick 512-bit parameters, finish remaining computations, and sign the parameters.</p>

<p>The original client, unaware of the MitM attack, believes the server picked a DHE (not DHE_EXPORT) key exchange.  At this point, since the connection uses insecure ciphers, the attacker can easily recover the connection key.  Over 8% of top one million HTTPS websites are vulnerable.</p>

<p>It&rsquo;s now believed that in addition to export Diffie-Hellman, non-export Diffie-Hellman is possibly vulnerable to <a href="http://www.spiegel.de/international/germany/inside-the-nsa-s-war-on-internet-security-a-1010361.html">nation-state surveillance</a>.  Researchers speculate that the NSA has completed the pre computation (estimated at 48 million core years) of a few common 1024-bit parameters (non unique prime numbers that are shared by many servers) and can now easily break new discrete logarithms.  If the cryptography algorithm is broken, that means that forward security is no longer maintained.</p>

<p><a href="https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_configurations">Mozilla</a> and <a href="https://weakdh.org/sysadmin.html">others</a> recommend disabling any export ciphers and using a large prime, preferably unique, for Diffie-Hellman key exchanges.</p>
<div class="highlight"><pre class="chroma"><code class="language-bash" data-lang="bash"><span class="c1"># generate unique prime for Diffie Hellman Exchanges
</span><span class="c1"></span>openssl dhparam -out dhparam.pem <span class="m">4096</span></code></pre></div><div class="highlight"><pre class="chroma"><code class="language-Nginx" data-lang="Nginx"><span class="c1"># prefer Eliptic Curve Diffie Hellman Exchanges
</span><span class="c1"></span><span class="k">ssl_ciphers</span> <span class="s">&#39;ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA&#39;</span><span class="p">;</span>

<span class="k">ssl_prefer_server_ciphers</span> <span class="no">on</span><span class="p">;</span>

<span class="c1"># uniquely generated prime
</span><span class="c1"></span><span class="k">ssl_dhparam</span> <span class="s">DHPARAMS</span> <span class="s">LOCATION</span><span class="p">;</span></code></pre></div>
<h2 id="finished-nginx-server-block-configuration">Finished Nginx Server Block Configuration</h2>
<div class="highlight"><pre class="chroma"><code class="language-Nginx" data-lang="Nginx"><span class="k">server</span> <span class="p">{</span>
    <span class="c1"># listen on port 443 using ssl and spdy
</span><span class="c1"></span>    <span class="kn">listen</span>                  <span class="mi">443</span> <span class="s">ssl</span> <span class="s">spdy</span> <span class="s">default_server</span><span class="p">;</span>
    <span class="kn">ssl_certificate</span>         <span class="s">CERTIFICATE</span> <span class="s">LOCATION</span><span class="p">;</span>
    <span class="kn">ssl_certificate_key</span>     <span class="s">KEY</span> <span class="s">LOCATION</span><span class="p">;</span>
    <span class="kn">ssl_session_cache</span>       <span class="s">shared:SSL:20m</span><span class="p">;</span>
    <span class="kn">ssl_session_timeout</span>     <span class="mi">10m</span><span class="p">;</span>
    <span class="kn">ssl_protocols</span>           <span class="s">TLSv1</span> <span class="s">TLSv1.1</span> <span class="s">TLSv1.2</span><span class="p">;</span>
    <span class="c1"># recommended secure ciphers
</span><span class="c1"></span>    <span class="kn">ssl_prefer_server_ciphers</span> <span class="no">on</span><span class="p">;</span>
    <span class="kn">ssl_ciphers</span> <span class="s">&#39;ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA&#39;</span><span class="p">;</span>
    <span class="c1"># large prime for Diffie-Hellman
</span><span class="c1"></span>    <span class="kn">ssl_dhparam</span> <span class="s">DHPARAMS</span> <span class="s">LOCATION</span><span class="p">;</span>
    <span class="kn">keepalive_timeout</span>       <span class="mi">70</span><span class="p">;</span>
    <span class="c1"># HTTP Strict Transport Security
</span><span class="c1"></span>    <span class="kn">add_header</span> <span class="s">Strict-Transport-Security</span> <span class="s">&#34;max-age=31536000</span><span class="p">;</span> <span class="kn">includeSubDomains&#34;</span><span class="p">;</span>
    <span class="c1"># key pinning
</span><span class="c1"></span>    <span class="kn">add_header</span> <span class="s">Public-Key-Pins</span> <span class="s">&#39;pin-sha256=&#34;BASE64-KEY-INFORMATION&#34;</span><span class="p">;</span> <span class="kn">pin-sha256=&#34;BASE64-KEY-INFORMATION&#34;</span><span class="p">;</span> <span class="kn">max-age=5184000</span><span class="p">;</span> <span class="kn">includeSubDomains&#39;</span><span class="p">;</span>

    <span class="kn">location</span> <span class="s">/</span> <span class="p">{</span>
        <span class="kn">root</span> <span class="s">/home/www/public_html/</span><span class="p">;</span>
        <span class="kn">index</span> <span class="s">index.html</span><span class="p">;</span>
        <span class="p">}</span>
    <span class="p">}</span></code></pre></div>
</div>

    <div class="footer">
      <div class="end mirango">
        
<a href="https://www.mirango.io/"><span class="base05">[</span><span class="base08">m</span><span class="base09">i</span><span class="base0a">r</span><span class="base0b">a</span><span class="base0c">n</span><span class="base0d">g</span><span class="base0e">o</span><span class="base05">]</span></a><span class="copyright"> &copy; 2014 - 2018</span>
        <div class="name">
          Sean Walsh —
        </div>
        <div class="email">
          sdwalsh@mirango.io
        </div>
      </div>
      <div class="commit-data">
        <a href="https://bitbucket.org/sdwalsh/mirango"><span id="commit-hash"></span></a>
        <div>
          <span id="commit-date"></span>
        </div>
      </div>
    </div>
  </div>
</body>
</html>

<script type="text/javascript" src="/all.min.js"></script>

